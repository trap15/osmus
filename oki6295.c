/*
	OsMUS -- Osman music player
	OKI6295 support

Copyright (C) 2012-2013		Alex Marshall <trap15@raidenii.net>
Copyright (C) 1997		Buffoni Mirko

# This code is licensed to you under the terms of the MIT license;
# see file LICENSE or http://www.opensource.org/licenses/mit-license.php
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

#include "oki6295.h"

/* OKI ADPCM to PCM conversion courtesy of Buffoni Mirko */

int32_t signal;
int32_t step;
const int8_t index_shift[8] = { -1, -1, -1, -1, 2, 4, 6, 8 };
int diff_lookup[49*16];

static int base = -1;
static int end;
static int start;

void oki6295_init(void)
{
	int nib;
	int stepval;
	// nibble to bit map
	static const int8_t nbl2bit[16][4] =
	{
		{ 1, 0, 0, 0}, { 1, 0, 0, 1}, { 1, 0, 1, 0}, { 1, 0, 1, 1},
		{ 1, 1, 0, 0}, { 1, 1, 0, 1}, { 1, 1, 1, 0}, { 1, 1, 1, 1},
		{-1, 0, 0, 0}, {-1, 0, 0, 1}, {-1, 0, 1, 0}, {-1, 0, 1, 1},
		{-1, 1, 0, 0}, {-1, 1, 0, 1}, {-1, 1, 1, 0}, {-1, 1, 1, 1}
	};

	// loop over all possible steps
	for(step = 0; step <= 48; step++)
	{
		// compute the step value
		stepval = floor(16.0 * pow(11.0 / 10.0, (double)step));

		// loop over all nibbles and compute the difference
		for(nib = 0; nib < 16; nib++)
		{
			diff_lookup[step*16 + nib] = nbl2bit[nib][0] *
				(stepval   * nbl2bit[nib][1] +
				 stepval/2 * nbl2bit[nib][2] +
				 stepval/4 * nbl2bit[nib][3] +
				 stepval/8);
		}
	}

	signal = -2;
	step = 0;
}

void oki6295_fini(void)
{
}

static uint16_t oki6295_clock(int nib)
{
	// update the signal
	signal += diff_lookup[step*16 + (nib & 15)];

	// clamp to the maximum
	if(signal > 2047)
		signal = 2047;
	else if(signal < -2048)
		signal = -2048;

	// adjust the step size and clamp
	step += index_shift[nib & 7];
	if(step > 48)
		step = 48;
	else if(step < 0)
		step = 0;

	// return the signal
	return signal;
}

const uint8_t _voltbl[16] = {
	0x20,   //   0 dB
	0x16,   //  -3.2 dB
	0x10,   //  -6.0 dB
	0x0b,   //  -9.2 dB
	0x08,   // -12.0 dB
	0x06,   // -14.5 dB
	0x04,   // -18.0 dB
	0x03,   // -20.5 dB
	0x02,   // -24.0 dB
	0x00,
	0x00,
	0x00,
	0x00,
	0x00,
	0x00,
	0x00,
};

size_t oki6295_conv(void *in, uint32_t base, int vol, int16_t *buf)
{
	int i, k;
	size_t samples;
	uint8_t *src = in;
	int16_t smp, osmp;
	uint32_t start, end;

	start = (src[base+0] << 16) |
		(src[base+1] << 8) |
		(src[base+2] << 0);
	end =	(src[base+3] << 16) |
		(src[base+4] << 8) |
		(src[base+5] << 0);
	start &= 0x3FFFF;
	end &= 0x3FFFF;
	if(base & ~0x3FFFF) {
		start |= (base & ~0x3FFFF);
		end |= (base & ~0x3FFFF);
	}

	samples = (end - start) * 2;
	src += start;
	for(i = 0; i < samples; i++) {
		int nib = src[i>>1];
		if(!(i & 1)) nib >>= 4;
		nib &= 0xF;

		smp = oki6295_clock(nib) * _voltbl[vol];
		buf[i] = smp;
	}
	return samples;
}

