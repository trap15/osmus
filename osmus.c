/*
	OsMUS -- Osman music player
	Main

Copyright (C) 2012-2013		Alex Marshall "trap15" <trap15@raidenii.net>

# This code is licensed to you under the terms of the MIT license;
# see file LICENSE or http://www.opensource.org/licenses/mit-license.php
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "oki6295.h"

#define BITSWAP32 SWAP32
#define SWAP32(v, \
	b31,b30,b29,b28, b27,b26,b25,b24, \
	b23,b22,b21,b20, b19,b18,b17,b16, \
	b15,b14,b13,b12, b11,b10,b9, b8,  \
	b7, b6 ,b5 ,b4 , b3 ,b2 ,b1 ,b0) ( \
((((v)>>b0 ) & 1) <<  0) | \
((((v)>>b1 ) & 1) <<  1) | \
((((v)>>b2 ) & 1) <<  2) | \
((((v)>>b3 ) & 1) <<  3) | \
((((v)>>b4 ) & 1) <<  4) | \
((((v)>>b5 ) & 1) <<  5) | \
((((v)>>b6 ) & 1) <<  6) | \
((((v)>>b7 ) & 1) <<  7) | \
((((v)>>b8 ) & 1) <<  8) | \
((((v)>>b9 ) & 1) <<  9) | \
((((v)>>b10) & 1) << 10) | \
((((v)>>b11) & 1) << 11) | \
((((v)>>b12) & 1) << 12) | \
((((v)>>b13) & 1) << 13) | \
((((v)>>b14) & 1) << 14) | \
((((v)>>b15) & 1) << 15) | \
((((v)>>b16) & 1) << 16) | \
((((v)>>b17) & 1) << 17) | \
((((v)>>b18) & 1) << 18) | \
((((v)>>b19) & 1) << 19) | \
((((v)>>b20) & 1) << 20) | \
((((v)>>b21) & 1) << 21) | \
((((v)>>b22) & 1) << 22) | \
((((v)>>b23) & 1) << 23) | \
((((v)>>b24) & 1) << 24) | \
((((v)>>b25) & 1) << 25) | \
((((v)>>b26) & 1) << 26) | \
((((v)>>b27) & 1) << 27) | \
((((v)>>b28) & 1) << 28) | \
((((v)>>b29) & 1) << 29) | \
((((v)>>b30) & 1) << 30) | \
((((v)>>b31) & 1) << 31))

size_t cpusz, okisz;
uint8_t *cpudat, *okidat;

static uint8_t r8(uint32_t addr)
{
	return cpudat[addr];
}

static uint16_t r16(uint32_t addr)
{
	return	(r8(addr+1) <<  8) |
		(r8(addr+0) <<  0);
}

static uint32_t r32(uint32_t addr)
{
	return	(r8(addr+3) << 24) |
		(r8(addr+2) << 16) |
		(r8(addr+1) <<  8) |
		(r8(addr+0) <<  0);
}

static void w8(void *buf, uint32_t addr, uint8_t data)
{
	uint8_t* dat = buf;
	dat[addr] = data;
}

static void w16(void *buf, uint32_t addr, uint16_t data)
{
	w8(buf, addr+0, data>>0);
	w8(buf, addr+1, data>>8);
}

static void w32(void *buf, uint32_t addr, uint32_t data)
{
	w8(buf, addr+0, data>> 0);
	w8(buf, addr+1, data>> 8);
	w8(buf, addr+2, data>>16);
	w8(buf, addr+3, data>>24);
}

static void _load_file(size_t *sz, uint8_t **buf, const char *fn)
{
	FILE *fp;
	fp = fopen(fn, "rb");

	fseek(fp, 0, SEEK_END);
	*sz = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	*buf = malloc(*sz);

	fread(*buf, *sz, 1, fp);
	fclose(fp);
}

static void _decrypt_cpu(uint8_t *buf, size_t sz)
{
	size_t i, l, k;
	uint8_t *dec = malloc(sz);
	for(i = 0; i < sz/4; i++) {
		l = (i & 0xFF0000) | 0x92C6;

		if(i & (1<< 0)) l ^= 0xCE4A;
		if(i & (1<< 1)) l ^= 0x4DB2;
		if(i & (1<< 2)) l ^= 0xEF60;
		if(i & (1<< 3)) l ^= 0x5737;
		if(i & (1<< 4)) l ^= 0x13DC;
		if(i & (1<< 5)) l ^= 0x4BD9;
		if(i & (1<< 6)) l ^= 0xA209;
		if(i & (1<< 7)) l ^= 0xD996;
		if(i & (1<< 8)) l ^= 0xA700;
		if(i & (1<< 9)) l ^= 0xECA0;
		if(i & (1<<10)) l ^= 0x7529;
		if(i & (1<<11)) l ^= 0x3100;
		if(i & (1<<12)) l ^= 0x33B4;
		if(i & (1<<13)) l ^= 0x6161;
		if(i & (1<<14)) l ^= 0x1EEF;
		if(i & (1<<15)) l ^= 0xF5A5;

		k = r32(l*4);

		if(i & (1<< 2)) k ^= (1<<22) | (1<<26);
		if(i & (1<< 3)) k ^= (1<< 2) | (1<<30);
		if(i & (1<< 4)) k ^= (1<<15) | (1<<18);
		if(i & (1<< 5)) k ^= (1<< 7) | (1<< 9);
		if(i & (1<< 6)) k ^= (1<< 6) | (1<<21);
		if(i & (1<< 7)) k ^= (1<<24) | (1<<27);
		if(i & (1<< 8)) k ^= (1<< 8) | (1<<12);
		if(i & (1<< 9)) k ^= (1<<13) | (1<<29);
		if(i & (1<<10)) k ^= (1<< 1) | (1<< 5);
		if(i & (1<<11)) k ^= (1<<17) | (1<<19);
		if(i & (1<<12)) k ^= (1<<14) | (1<<28);
		if(i & (1<<13)) k ^= (1<<10) | (1<<16);
		if(i & (1<<14)) k ^= (1<< 4) | (1<<31);
		if(i & (1<<15)) k ^= (1<< 0) | (1<< 3);
		if(i & (1<<16)) k ^= (1<<20) | (1<<25);
		if(i & (1<<17)) k ^= (1<<11) | (1<<23);

		switch(i & 3) {
			case 0:	k = SWAP32(k ^ 0xEC63197A,
					 1,  4,  7, 28, 22, 18, 20,  9,
					16, 10, 30,  2, 31, 24, 19, 29,
					 6, 21, 23, 11, 12, 13,  5,  0,
					 8, 26, 27, 15, 14, 17, 25,  3); break;
			case 1:	k = SWAP32(k ^ 0x58A5A55F,
					14, 23, 28, 29,  6, 24, 10,  1,
					 5, 16,  7,  2, 30,  8, 18,  3,
					31, 22, 25, 20, 17,  0, 19, 27,
					 9, 12, 21, 15, 26, 13,  4, 11); break;
			case 2:	k = SWAP32(k ^ 0xE3A65F16,
					19, 30, 21,  4,  2, 18, 15,  1,
					12, 25,  8,  0, 24, 20, 17, 23,
					22, 26, 28, 16,  9, 27,  6, 11,
					31, 10,  3, 13, 14,  7, 29,  5); break;
			case 3:	k = SWAP32(k ^ 0x28D93783,
					30,  6, 15,  0, 31, 18, 26, 22,
					14, 23, 19, 17, 10,  8, 11, 20,
					 1, 28,  2,  4,  9, 24, 25, 27,
					 7, 21, 13, 29,  5,  3, 16, 12); break;
		}
		w32(dec, i*4, k);
	}
	memcpy(buf, dec, sz);
	free(dec);
}

static int _decrypt_oki(uint8_t *buf, size_t sz)
{
	size_t i, l;
	uint8_t *dec = malloc(sz);
	for(i = 0; i < sz; i++) {
		l  = SWAP32(i,
			31,30,29,28,27,26,25,24,
			23,22,21, 0,20,19,18,17,
			16,15,14,13,12,11,10, 9,
			 8, 7, 6, 5, 4, 3, 2, 1);
		dec[l] = buf[i];
	}
	memcpy(buf, dec, sz);
	free(dec);
}

#define HISTCNT 16
#define SCALER (32.0f)

static int16_t smphist[HISTCNT];

static int use_interp = 0;
static int genweight = 1;
static float weighter[HISTCNT+2] = {
	0.0f,
	1.000f/SCALER/SCALER/SCALER/SCALER/SCALER/SCALER/SCALER/SCALER,
	1.000f/SCALER/SCALER/SCALER/SCALER/SCALER/SCALER/SCALER,
	1.000f/SCALER/SCALER/SCALER/SCALER/SCALER/SCALER,
	1.000f/SCALER/SCALER/SCALER/SCALER/SCALER,
	1.000f/SCALER/SCALER/SCALER/SCALER,
	1.000f/SCALER/SCALER/SCALER,
	1.000f/SCALER/SCALER,
	1.000f/SCALER,
	1.000f,
	1.000f/SCALER,
	1.000f/SCALER/SCALER,
	1.000f/SCALER/SCALER/SCALER,
	1.000f/SCALER/SCALER/SCALER/SCALER,
	1.000f/SCALER/SCALER/SCALER/SCALER/SCALER,
	1.000f/SCALER/SCALER/SCALER/SCALER/SCALER/SCALER,
	1.000f/SCALER/SCALER/SCALER/SCALER/SCALER/SCALER/SCALER,
	0.0f,
};
static float weights0[HISTCNT];
static float weights1[HISTCNT];
static float weights2[HISTCNT];
//static float weights3[HISTCNT];

static void gen_weights()
{
	int i;
	genweight = 0;
	for(i = 0; i < HISTCNT; i++) {
		weights0[i] = weighter[i+2]*0.2500f + weighter[i+1]*0.5000f + weighter[i+0]*0.2500f;
		weights1[i] = weighter[i+2]*0.1250f + weighter[i+1]*0.5000f + weighter[i+0]*0.3750f;
		weights2[i] = weighter[i+2]*0.0000f + weighter[i+1]*0.5000f + weighter[i+0]*0.5000f;
	}
}

static void do_interp(int16_t *buf, size_t bufsz, FILE *fp)
{
	size_t i, l;
	int16_t smp, smp0,smp1,smp2;
	if(genweight) {
		gen_weights();
	}
	for(i = 0; i < bufsz; i++) {
		smp = buf[i];

		memcpy(smphist, smphist+1, (HISTCNT-1) * 2);
		smphist[HISTCNT-1] = smp;

		if(use_interp) {
			smp0 = 0;
			for(l = 0; l < HISTCNT; l++) {
				smp0 += smphist[l] * weights0[l] / 2;
			}
			smp1 = 0;
			for(l = 0; l < HISTCNT; l++) {
				smp1 += smphist[l] * weights1[l] / 2;
			}
			smp2 = 0;
			for(l = 0; l < HISTCNT; l++) {
				smp2 += smphist[l] * weights2[l] / 2;
			}
			fwrite(&smp0, 2, 1, fp);
			fwrite(&smp1, 2, 1, fp);
			fwrite(&smp2, 2, 1, fp);
		}else{
			fwrite(&smp, 2, 1, fp);
		}
	}
}

static void osmus_rip_song(uint32_t tbladdr, int songid, FILE *fp)
{
	uint32_t songbase = r32(tbladdr + (songid<<2));
	uint32_t ea = songbase;
	int16_t *buf;
	size_t bufsz;
	int vol = r8(ea++);
	int smpid;
	int loop = 2;
	int bank, base;
	buf = malloc(2 * 65536 * 4);
	for(;;) {
		smpid = r8(ea++);
		if(smpid == 0xFE) {
			if(--loop) {
				ea = songbase+1;
				continue;
			}else{
				break;
			}
		}else if(smpid == 0xFF) {
			break;
		}
		bank = smpid >> 5;
		base = smpid & 0x1F;
		base *= 8;
		bank *= 0x40000;
		bufsz = oki6295_conv(okidat + bank, base, vol, buf);
		do_interp(buf, bufsz, fp);
	}
	free(buf);
}

#define W8(x)  do { uint8_t  tmp = (x); fwrite(&tmp, 1, 1, fp); } while(0)
#define W16(x) do { uint16_t tmp = (x); fwrite(&tmp, 2, 1, fp); } while(0)
#define W32(x) do { uint32_t tmp = (x); fwrite(&tmp, 4, 1, fp); } while(0)
#define WID(x) do { uint8_t *tmp = (x); fwrite( tmp, 4, 1, fp); } while(0)

static size_t _fixup_size = 0;
static size_t _fixup_smps = 0;

static void write_basic_wav_head(FILE *fp)
{
	WID("RIFF"); _fixup_size = ftell(fp); W32(0); // To be fixed up later
	WID("WAVE");
	WID("fmt "); W32(16);
		W16(0x0001);
		W16(1);
	if(use_interp) {
		W32(32220000/16/44/2); // sample rate
		W32(32220000/16/44/2*2);
	}else{
		W32(32220000/16/132/2); // sample rate
		W32(32220000/16/132/2*2);
	}
		W16(2);
		W16(16);
	WID("data"); _fixup_smps = ftell(fp); W32(0);
}

static void write_finished_wav_head(FILE *fp)
{
	size_t pos = ftell(fp);
	fseek(fp, _fixup_size, SEEK_SET);
	W32(pos - 4);
	fseek(fp, _fixup_smps, SEEK_SET);
	W32(pos - 44);
}

int main(int argc, const char *argv[])
{
	FILE *cpufp, *okifp, *wavfp;

	printf("OsMUS (C)2012-2013 Alex Marshall \"trap15\"\n");
	if(argc < 5) {
		printf(	"Usage:\n"
			"\t%s songid sa00-0.1e mcf-05.12f out.wav [i]\n", argv[0]);
		return EXIT_FAILURE;
	}
	if(argc > 5) {
		use_interp = 1;
	}

	_load_file(&cpusz, &cpudat, argv[2]);
	_load_file(&okisz, &okidat, argv[3]);

	_decrypt_cpu(cpudat, cpusz);
	_decrypt_oki(okidat, okisz);

	wavfp = fopen(argv[4], "wb+");
	write_basic_wav_head(wavfp);

	oki6295_init();
	osmus_rip_song(0xEC40, atoi(argv[1]), wavfp);
	oki6295_fini();

	write_finished_wav_head(wavfp);
	fclose(wavfp);

	free(cpudat);
	free(okidat);

	return EXIT_SUCCESS;
}

