OBJECTS   = osmus.o oki6295.o
TARGET    = osmus
LIBS      = -lm
INCLUDE   = -I.
CFLAGS    = $(INCLUDE) -g
LDFLAGS   = $(LIBS) -g
CLEANED   = $(OBJECTS) $(TARGETS)

.PHONY: all clean

all: $(OBJECTS) $(TARGET)
%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

$(TARGET): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

clean:
	$(RM) $(CLEANED)

